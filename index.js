// bài 1

function sapXep() {
  var number1 = document.getElementById("txt-number1").value * 1;
  var number2 = document.getElementById("txt-number2").value * 1;
  var number3 = document.getElementById("txt-number3").value * 1;

  if (number1 < number2 && number1 < number3) {
    if (number2 < number3) {
      document.getElementById("result1").innerHTML = ` 
        ${number1} < ${number2} < ${number3} 
        `;
    } else {
      document.getElementById("result1").innerHTML = ` 
        ${number1} < ${number3} < ${number2} 
        `;
    }
  } else if (number2 < number1 && number2 < number3) {
    if (number1 < number3) {
      document.getElementById("result1").innerHTML = ` 
        ${number2} < ${number1} < ${number3} 
        `;
    } else {
      document.getElementById("result1").innerHTML = ` 
        ${number2} < ${number3} < ${number1} 
        `;
    }
  } else if (number3 < number1 && number3 < number2) {
    if (number1 < number2) {
      document.getElementById("result1").innerHTML = ` 
        ${number3} < ${number1} < ${number2} 
        `;
    } else {
      document.getElementById("result1").innerHTML = ` 
        ${number3} < ${number2} < ${number1} 
        `;
    }
  }
}

// bài 2

function sayHello() {
  var listFamily = document.getElementById("listFamily").value;

  switch (listFamily) {
    case "dad":
      document.getElementById("result2").innerHTML = ` Hello Bố! `;
      break;
    case "mom":
      document.getElementById("result2").innerHTML = ` Hello Mẹ! `;
      break;
    case "brother":
      document.getElementById("result2").innerHTML = ` Hello Anh Trai! `;
      break;
    case "sister":
      document.getElementById("result2").innerHTML = ` Hello Em Gái! `;
      break;
  }
}

// bài 3

function countNumber() {
  var num1 = document.getElementById("txt-num1").value * 1;
  var num2 = document.getElementById("txt-num2").value * 1;
  var num3 = document.getElementById("txt-num3").value * 1;
  var count = 0;

  if (num1 % 2 == 0) {
    count++;
  }

  if (num2 % 2 == 0) {
    count++;
  }

  if (num3 % 2 == 0) {
    count++;
  }
  document.getElementById("result3").innerHTML = ` 
  Số chẵn là : ${count} ;
  Số lẻ là : ${3 - count}
  `;
}

// bài 4

function duDoan() {
  var a = document.getElementById("txt-canh-1").value * 1;
  var b = document.getElementById("txt-canh-2").value * 1;
  var c = document.getElementById("txt-canh-3").value * 1;

  if (a == b && a == c && b == c) {
    document.getElementById("result4").innerHTML = ` Hình tam giác đều `;
  } else if (a == b || a == c || b == c) {
    document.getElementById("result4").innerHTML = ` Hình tam giác cân `;
  } else if (
    c * c == a * a + b * b ||
    a * a == c * c + b * b ||
    b * b == a * a + c * c
  ) {
    document.getElementById("result4").innerHTML = ` Hình tam giác vuông `;
  } else {
    document.getElementById("result4").innerHTML = ` Một loại tam giác khác `;
  }
}
